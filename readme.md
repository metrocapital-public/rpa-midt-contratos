<br />
<div align="center">
  <a href="https://www.python.org">
    <img src="images/python-240.png" alt="Logo" width="100" height="100">
  </a>
</div>

</br>

# RPA Carga Contratos - MiDT

Este es un proyecto RPA desarrollado con estándares MetroCapital y Rocketbot, para carga de contratos en portal MiDT.

</br>

## Installation

A continuación se muestra un ejemplo de cómo puede montar su proyecto RPA.

1. Clonar el repositorio mediante git o descargar .zip
   ```sh
   git clone git@gitlab.com:metrocapital-public/rpa-midt-contratos.git <optional new-name>
   ```

2. Instalar pip packages
   ```sh
   pip install -r requirements.txt
   ```

3. Crear archivo app.bat
   ```
   @echo off
   title your-title

   :: RPA Rocketbot
   cd C:\Rocketbot\Rocketbot
   rocketbot.exe -db="C:\Rocketbot\Rocketbot\robot.db" -start="dirtrab"

   timeout 10
   exit
   ```

## Author
* Jorge Lagos - Engineer Python & Full Stack DEV